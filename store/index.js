import axios from 'axios'

export const state = () => ({
  isMenuCollapsed: false,
})

export const actions = {
  async nuxtServerInit({ commit }) {
    const { data } = await axios.get('http://79.244.156.1/test_data.json')
    commit('sources/setData', data)
  },
}

export const mutations = {
  changeMenuState(state) {
    state.isMenuCollapsed = !state.isMenuCollapsed
  },
}
