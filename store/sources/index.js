import {
  getMappedLearners,
  getMappedMaterials,
  getSortedResults,
  getInitialData,
  getSearchResultsByLearners,
  getSearchResultsByMaterials,
} from './utils'
import { orderingList, viewTypes } from './constants'

export const state = () => ({
  learners: [],
  materials: [],
  searchQueryLearners: '',
  searchQueryMaterials: '',
  orderingId: 0,
  viewTypeId: 0,
  selectedMaterials: [],
})

export const getters = {
  sorting: (state) => orderingList[state.orderingId],
  learners: (state) => {
    const learners = getSearchResultsByLearners(
      state.searchQueryLearners,
      state.learners
    ).filter((learner) =>
      state.selectedMaterials.length > 0
        ? learner.materials.find((material) =>
            state.selectedMaterials.includes(material.id)
          )
        : true
    )
    const sorting = orderingList[state.orderingId]
    const sortedLearners = getSortedResults(sorting, learners)
    const mappedLearners = getMappedLearners(sortedLearners, state.materials)
    return getSearchResultsByMaterials(
      state.searchQueryMaterials,
      mappedLearners
    )
  },
  materials: (state) => {
    return getMappedMaterials(state.materials, state.learners)
  },
  viewTypes: () => viewTypes,
  viewType: (state) => {
    return viewTypes[state.viewTypeId]
  },
}

export const mutations = {
  setData(state, { learners: learnersData, materials: materialsData }) {
    const { learners, materials } = getInitialData(learnersData, materialsData)
    state.materials = materials
    state.learners = learners
  },
  setNextOrderingType(state, text) {
    state.orderingId = (state.orderingId + 1) % orderingList.length
  },
  studentFilter(state, text) {
    state.searchQueryLearners = text
  },
  selectMaterial(state, id) {
    if (state.selectedMaterials.includes(id)) {
      state.selectedMaterials = state.selectedMaterials.filter(
        (material) => material !== id
      )
    } else {
      state.selectedMaterials.push(id)
    }
  },
  materialFilter(state, text) {
    state.searchQueryMaterials = text
  },
  changeViewType(state, id) {
    if (state.viewTypeId === 1 && id === 0) {
      state.selectedMaterials = []
    }
    state.viewTypeId = id
  },
}
