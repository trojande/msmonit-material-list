export const orderingList = [
  {
    order: 'asc',
    name: 'Имя Фамилия',
    getName: (student) => `${student.firstname} ${student.lastname}`,
  },
  {
    order: 'desc',
    name: 'Имя Фамилия',
    getName: (student) => `${student.firstname} ${student.lastname}`,
  },
  {
    order: 'asc',
    name: 'Фамилия Имя',
    getName: (student) => `${student.lastname} ${student.firstname}`,
  },
  {
    order: 'desc',
    name: 'Фамилия Имя',
    getName: (student) => `${student.lastname} ${student.firstname}`,
  },
]

export const viewTypes = [
  {
    id: 0,
    name: 'По ученикам',
  },
  {
    id: 1,
    name: 'По групповым презентациям',
  },
]
