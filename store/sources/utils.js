import sortBy from 'lodash.sortby'

export const getSearchResultsByLearners = (searchQuery, learners) => {
  if (!searchQuery) {
    return learners
  }
  const filteredLearners = learners.filter(
    (student) =>
      student.firstname.toLowerCase().includes(searchQuery.toLowerCase()) ||
      student.lastname.toLowerCase().includes(searchQuery.toLowerCase())
  )
  return filteredLearners
}

export const getSearchResultsByMaterials = (searchQuery, learners) => {
  if (!searchQuery) {
    return learners
  }
  const filteredLearners = learners.filter((student) =>
    student.materials.some((material) =>
      material.name.toLowerCase().includes(searchQuery.toLowerCase())
    )
  )
  return filteredLearners
}

export const getSortedResults = (sorting, learners) => {
  const mappedLearners = learners.map((student) => ({
    ...student,
    name: sorting.getName(student),
  }))
  const sortedLearners = sortBy(mappedLearners, 'name')
  if (sorting.order === 'desc') {
    return sortedLearners.reverse()
  }
  return sortedLearners
}

export const getMaterialUsingCount = (material, learners) => {
  return learners.filter((learner) =>
    learner.materials.find((item) => item.id === material.id)
  ).length
}

export const getMappedLearners = (learners, materials) => {
  return learners.map((learner) => ({
    ...learner,
    materials: learner.materials.map((material) => {
      return {
        ...material,
        ...materials.find((m) => m.id === material.id),
        used: getMaterialUsingCount(material, learners),
      }
    }),
  }))
}

export const getMappedMaterials = (materials, learners) => {
  return materials.map((material) => {
    const learner = learners.find((l) =>
      l.materials.some((m) => m.id === material.id)
    )
    return {
      ...material,
      used: getMaterialUsingCount(material, learners),
      active: !!learner.materials.find((m) => m.id === material.id).score,
    }
  })
}

export const getInitialData = (learners, materials) => {
  // ids are not valid therefore we will use index as id
  const mappedLearners = learners.map((learner, idx) => ({
    ...learner,
    id: idx,
    materials: Object.keys(learner.materials || []).map((id) => ({
      id: parseInt(id),
      ...learner.materials[id],
    })),
  }))
  const mappedMaterials = materials.map((material) => ({
    ...material,
    id: parseInt(material.id, 10),
  }))

  return {
    learners: mappedLearners,
    materials: mappedMaterials,
  }
}
